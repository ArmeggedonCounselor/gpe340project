﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour {
	[Header("Data")]
	[SerializeField][Tooltip("This variable sets the time that this object will be present in the scene. If this <= 0, the object does not automatically disappear.")] private float lifespan;
	[SerializeField][Tooltip("This variable sets the number of degrees the object rotates through each Update.")] private float spinSpeed;

	[Header("Components")]
	[HideInInspector]public Transform tf;

	// Use this for initialization
	public virtual void Start () {
		if (lifespan > 0) {
			Destroy (gameObject, lifespan);
		}
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	public virtual void Update () {

		tf.Rotate (Vector3.up, spinSpeed);
	}

	public void OnTriggerEnter(Collider other){
		OnPickup (other.gameObject);
	}

	public virtual void OnPickup (GameObject target){

		Destroy (gameObject);
	}
}
