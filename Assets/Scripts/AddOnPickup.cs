﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddOnPickup : Pickup {

	[SerializeField]
	[Tooltip("The Add-On this pickup gives the player.")] private AddOn pickup;

	// Use this for initialization
	public override void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
	}

	public override void OnPickup (GameObject target)
	{
		target.SendMessage ("EquipAddOn", pickup, SendMessageOptions.DontRequireReceiver);
		PlayerPawn tempPawn = target.GetComponent<PlayerPawn> ();
		if (tempPawn != null) {
			base.OnPickup (target);
		}
	}
}
