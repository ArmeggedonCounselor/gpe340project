﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : Weapon {

	// Use this for initialization
	void Start () {
		RightHand = true;
		LeftHand = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void Fire(){
		// Hold onto this for later, but for now, use regular bullets.
		/*Ray fireRay = new Ray();
		fireRay.origin = firePosition.position;
		fireRay.direction = firePosition.forward;

		RaycastHit hitData;
		if (Physics.Raycast (fireRay, out hitData, fireDistance)) {
			GameObject hitObject = hitData.collider.gameObject;
			Health hitObjectHealth = hitObject.GetComponent<Health> ();
			if (hitObjectHealth != null) {
				hitObjectHealth.TakeDamage (-1 * bulletDamage);
			}
			GameObject particles = Instantiate (hitParticlePrefab, hitObject.transform.position, Quaternion.identity) as GameObject;
			Destroy (particles, particleLifespan);

			GameObject line = Instantiate (linePrefab, Vector3.zero, Quaternion.identity) as GameObject;
			LineRenderer lr = line.GetComponent<LineRenderer> ();
			lr.SetPosition (0, firePosition.position);
			lr.SetPosition (1, hitData.point);
			Destroy (line, particleLifespan);
		} else {
			GameObject line = Instantiate (linePrefab, Vector3.zero, Quaternion.identity) as GameObject;
			LineRenderer lr = line.GetComponent<LineRenderer> ();
			lr.SetPosition (0, firePosition.position);
			lr.SetPosition (1, (firePosition.position + (firePosition.forward * fireDistance)));
			Destroy (line, particleLifespan);
		}
			*/
		// Find the point "spread" units in front of my gun fire position.

		float xSpread = Random.Range (-1f, 1f);
		float ySpread = Random.Range (-1f, 1f);

		Vector3 randomSpread = new Vector3 (xSpread, ySpread, 0.0f).normalized * spread;

		Quaternion rot = Quaternion.Euler (randomSpread) * firePosition.rotation;

		// Instantiate bullet.
		GameObject bullet = Instantiate (bulletPrefab, firePosition.position, rot) as GameObject;

		bullet.BroadcastMessage ("SetDamage", bulletDamage);
		// Push bullet forward.
		Rigidbody bulletBody = bullet.GetComponent<Rigidbody> ();
		bulletBody.AddForce (bullet.transform.forward * bulletForce);

		// Bullet Lifespan.
		Destroy (bullet, bulletLifespan);
	}
}
