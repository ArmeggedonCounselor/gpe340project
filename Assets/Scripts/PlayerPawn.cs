﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn {

	[HideInInspector]
	public Animator anim;
	[HideInInspector]
	public CapsuleCollider coll;
	[HideInInspector]
	public RagdollController ragdoll;

	[SerializeField] private KeyCode sprintKey;

	[Header ("Data")]
	[SerializeField]
	[Tooltip("This is the speed in degrees per frame that the pawn turns.")]
	private float turnSpeed;


	public float walkSpeed;
	public float runSpeed;
	public float sprintSpeed;
	public bool isRun;
	public bool isSprint;

	[Header("Weapon Info")]
	public Weapon currentWeapon;
	public List<Weapon> weaponInventory;
	public Transform weaponPoint;

	[Header("Add-On Info")]
	public AddOn leftShoulder;
	public AddOn rightShoulder;
	public Transform leftAddNode;
	public Transform rightAddNode;

	private bool equippedWeapon = false;
	private int weaponNumber = 0;



	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		coll = GetComponent<CapsuleCollider> ();
		ragdoll = GetComponent<RagdollController> ();
		weaponInventory = new List<Weapon> ();
	}
	
	// Update is called once per frame
	public void Update(){
		AimForward ();
	}

	public override void Move(Vector2 moveVector) {
		anim.SetFloat ("Horizontal",  moveVector.x); 
		anim.SetFloat ("Vertical", moveVector.y); 
	}

	public override void Die ()
	{
		Destroy (currentWeapon.gameObject);
		Destroy (rightShoulder.gameObject);
		Destroy (leftShoulder.gameObject);
		isDead = true;
		ragdoll.isRagdoll = true;
	}

	public override void RotateTowards(Vector3 target){
		// Find Vector to target.
		Vector3 targetVector = target - tf.position;

		// Find rotation that looks down vector.
		Quaternion targetRotation = Quaternion.LookRotation(targetVector);

		// Change my vector a little closer based on rotate speed.
		tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, turnSpeed);
	}

	public void AimForward(){
		if (currentWeapon != null) {
			currentWeapon.gameObject.GetComponent<Transform> ().forward = tf.forward;
		}
	}

	public override void Crouch(){
		// Check if the pawn is crouching.
		if (isCrouching) {
			coll.center = new Vector3 (0.0f, 0.9f, 0.0f);
			coll.height = 1.8f; // Probably shouldn't be hard coding this, buuut....
		} else {
			coll.center = new Vector3 (0.0f, 0.75f, 0.0f);
			coll.height = 1.5f; // .... Works for now, I suppose.
		}
		// Toggle isCrouching on or off.
		isCrouching = !isCrouching;

		// Match the "Crouch" parameter to isCrouching.
		anim.SetBool ("Crouch", isCrouching);
	}

	public override void EquipWeapon (Weapon weapon){
		// Instantiate the weapon.
		if (weapon != null) {
			if (equippedWeapon) {
				Destroy (currentWeapon.gameObject);
			}
			currentWeapon = Instantiate (weapon, weaponPoint);
			currentWeapon.OnEquip ();
			equippedWeapon = true;
		} else {
			currentWeapon = null;
			equippedWeapon = false;
		}

		//TODO: Other things when equipping a weapon.
	}

	public override void NextWeapon(bool direction){
		if (direction) {
			weaponNumber++;
			if (weaponNumber >= weaponInventory.Count) {
				weaponNumber = 0;
			}
		} else {
			weaponNumber--;
			if (weaponNumber < 0) {
				weaponNumber = weaponInventory.Count - 1;
			}
		}
		EquipWeapon (weaponInventory [weaponNumber]);
	}

	public void AddWeapon(Weapon weapon){
		weaponInventory.Add (weapon);
		if (weaponNumber == 0) {
			weaponNumber = weaponInventory.Count;
			EquipWeapon (weapon);
		}
	}

	public override void FireWeapon(){
		currentWeapon.PullTrigger ();
	}

	public void EquipAddOn(AddOn addon){
		if (rightShoulder != null) {
			if (leftShoulder != null) {
				Destroy (leftShoulder.gameObject);
			}
			AddOn temp = rightShoulder;
			leftShoulder = Instantiate (temp, leftAddNode);
			Destroy (rightShoulder.gameObject);
		}
		rightShoulder = Instantiate (addon, rightAddNode);
	}

	void OnAnimatorIK(){
		if (equippedWeapon) {
			if (currentWeapon.RightHand) {
				anim.SetIKPosition (AvatarIKGoal.RightHand, currentWeapon.RHPoint.position);
				anim.SetIKRotation (AvatarIKGoal.RightHand, currentWeapon.RHPoint.rotation);

				anim.SetIKPositionWeight (AvatarIKGoal.RightHand, 1.0f);
				anim.SetIKRotationWeight (AvatarIKGoal.RightHand, 1.0f);
			}

			if (currentWeapon.LeftHand) {
				anim.SetIKPosition (AvatarIKGoal.LeftHand, currentWeapon.LHPoint.position);
				anim.SetIKRotation (AvatarIKGoal.LeftHand, currentWeapon.LHPoint.rotation);

				anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, 1.0f);
				anim.SetIKRotationWeight (AvatarIKGoal.LeftHand, 1.0f);
			}
		}
		else {
			anim.SetIKPositionWeight (AvatarIKGoal.RightHand, 0.0f);
			anim.SetIKRotationWeight (AvatarIKGoal.RightHand, 0.0f);
			anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, 0.0f);
			anim.SetIKRotationWeight (AvatarIKGoal.LeftHand, 0.0f);
		}
	}
}
