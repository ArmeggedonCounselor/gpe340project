﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon {

	[Tooltip("The number of pellets this gun fires.")]public int pellets;

	// Use this for initialization
	void Start () {
		RightHand = true;
		LeftHand = true;
	}
	
	// Update is called once per frame
	void Update (){
		
	}
		
	public override void Fire ()
	{
		List<GameObject> bullets = new List<GameObject> ();
		for (int i = 0; i < pellets; i++) {
			// Find the point "spread" units in front of my gun fire position.

			float xSpread = Random.Range (-1f, 1f);
			float ySpread = Random.Range (-1f, 1f);

			Vector3 randomSpread = new Vector3 (xSpread, ySpread, 0.0f).normalized * spread;

			Quaternion rot = Quaternion.Euler (randomSpread) * firePosition.rotation;

			// Instantiate bullet.
			bullets.Add (Instantiate (bulletPrefab, firePosition.position, rot) as GameObject);
			bullets [i].BroadcastMessage ("SetDamage", bulletDamage);
		}

		for (int i = 0; i < pellets; i++) {
			// Push bullet forward.
			Rigidbody bulletBody = bullets [i].GetComponent<Rigidbody> ();
			bulletBody.AddForce (bullets [i].transform.forward * bulletForce);

			// Bullet Lifespan.
			Destroy (bullets [i], bulletLifespan);
		}
	}
}
