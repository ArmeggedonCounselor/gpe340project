﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
	public Pawn pawn;

	public Plane groundPlane;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (pawn != null) {
			Vector3 controllerInput = new Vector3 (Input.GetAxis ("Horizontal"), 0.0f, Input.GetAxis ("Vertical"));
			if (Input.GetKey(KeyCode.LeftShift)&& !pawn.isCrouching) {
				controllerInput = Vector3.ClampMagnitude (controllerInput, 0.5f);
			} else {
				controllerInput = Vector3.ClampMagnitude (controllerInput, 1.0f);
			}
			controllerInput = pawn.tf.InverseTransformDirection (controllerInput);

			pawn.Move (new Vector2(controllerInput.x, controllerInput.z));
		}
		if (Input.GetKeyDown (KeyCode.Q)) {
			pawn.NextWeapon (true);
		}

		if (Input.GetKeyDown (KeyCode.E)) {
			pawn.NextWeapon (false);
		}

		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			pawn.FireWeapon ();
		}

		if (!pawn.isDead) {
			RotateToMouse ();
		}
	}

	public void RotateToMouse (){

		// Define Ground plane
		groundPlane = new Plane(Vector3.up, pawn.tf.position);

		// Raycast through mouse position to ground plane
		float distToPlane;
		Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		groundPlane.Raycast (mouseRay, out distToPlane);
		// Find a point on the plane
		Vector3 pointOnPlane = mouseRay.GetPoint(distToPlane);

		// Rotate toward that point
		// TEMP: Change this to use "RotateTowards"
		pawn.RotateTowards(pointOnPlane);
	}
}
