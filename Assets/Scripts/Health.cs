﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
	
	public float health;
	public float maxHealth;
	[HideInInspector]
	public float healthPercent {
		get {
			return Mathf.Ceil ((health / maxHealth) * 100);
		}
	}

	void Start(){
		health = maxHealth;
	}
	/// <summary>
	/// Change health by some amount.
	/// </summary>
	/// <param name="amountOfDamage">How much to change health. Positive numbers heal, negative numbers harm.</param>
	public void TakeDamage(float amountOfDamage){
		health += amountOfDamage;
		if (health <= 0) {
			Die ();
		}
		if (health > maxHealth) {
			health = maxHealth;
		}
	}

	public void Die(){
		//TODO: Implement Die()
		Pawn pawn = GetComponent<Pawn>();

		if (pawn != null) {
			pawn.Die ();
		}
	}
}
