﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AIPawn : Pawn {
	[Header("Data")]
	[Tooltip("The speed at which this pawn moves.")]
	public float speed;
	[Tooltip("The speed at which this pawn turns.")]
	public float turnSpeed;
	[Tooltip("How long does this pawn wait before updating the position of its target.")]
	public float updateDelay;

	// Private Variables
	private float timer;
	private NavMeshAgent agent;
	private Transform target;
	private Vector3 startingPos;
	private Pawn targetPawn;



	// Use this for initialization
	void Start () {
		startingPos = tf.position;
		agent = GetComponent<NavMeshAgent> ();
		agent.speed = speed;
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		targetPawn = target.GetComponent<Pawn> ();
		timer = updateDelay;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (timer > updateDelay) {
			Move ();
			timer = 0;
		}
		if (targetPawn != null) {
			if (targetPawn.isDead) {
				target = null;
			}
		}
	}

	public override void Move(){
		if (target != null) {
			agent.SetDestination (target.position);
			RotateTowards (target.position);
		} else {
			agent.SetDestination (startingPos);
		}
	}

	public override void RotateTowards (Vector3 target)
	{
		Vector3 targetVector = target - tf.position;
		Quaternion targetRotation = Quaternion.LookRotation (targetVector);
		tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, turnSpeed);
	}

	public override void Die(){
		this.SendMessage ("AIDie", SendMessageOptions.DontRequireReceiver);
	}
}
