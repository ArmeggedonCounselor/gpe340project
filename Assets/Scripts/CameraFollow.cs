﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	[Header ("Target")]
	public GameObject character;

	private Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = transform.position - character.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = character.transform.position + offset;
	}
}
