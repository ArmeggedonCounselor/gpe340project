﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : Pickup {

	[SerializeField][Tooltip("The weapon that this pickup gives the player.")]private Weapon pickup;

	// Use this for initialization
	public override void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	public override void Update () {

		base.Update ();
	}

	public override void OnPickup (GameObject target)
	{
		PlayerPawn tempPlayer = target.GetComponent<PlayerPawn> ();
		if (tempPlayer != null) {
			tempPlayer.AddWeapon (pickup);
			base.OnPickup (target);

		} else {
			Pawn tempPawn = target.GetComponent<Pawn> ();
			if (tempPawn != null) {
				tempPawn.EquipWeapon (pickup);
				base.OnPickup (target);
			}
		}
	}
}
