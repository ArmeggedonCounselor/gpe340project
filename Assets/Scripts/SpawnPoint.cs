﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {
	public GameObject prefabToSpawn;
	private List<GameObject> spawnedObjects;
	public float respawnTime;
	private float countdown;
	public int maxSpawns = 1;
	private int currentSpawns;
	private Transform tf;

	// Use this for initialization
	void Start () {
		countdown = 0;
		currentSpawns = 0;
		tf = this.GetComponent<Transform> ();
		spawnedObjects = new List<GameObject> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(respawnTime != -1){
		 if (currentSpawns < maxSpawns) {
				countdown -= Time.deltaTime;
				if (countdown <= 0) {
					spawnedObjects.Add (Instantiate (prefabToSpawn, tf.position, tf.rotation) as GameObject);
					countdown = respawnTime;
				}
			}
		}
		if (respawnTime == -1) {
			spawnedObjects.Add (Instantiate (prefabToSpawn, tf.position, tf.rotation) as GameObject);
			Destroy (this.gameObject);
		}
		spawnedObjects.Remove (null);
		currentSpawns = spawnedObjects.Count;

	}

	void OnDrawGizmos(){
		// Change Color
		Gizmos.color = Color.yellow;

		// Draw a Sphere
		Gizmos.DrawWireSphere(GetComponent<Transform>().position, 1.0f);

		Gizmos.color = Color.blue;
		Gizmos.DrawRay (GetComponent<Transform> ().position, GetComponent<Transform> ().forward * 4);
	}
}
