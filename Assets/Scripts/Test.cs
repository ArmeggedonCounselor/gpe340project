﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

	public GameObject player;
	public Weapon weaponPrefab;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.H)) {
			player.BroadcastMessage ("TakeDamage", -5);
		}
		if (Input.GetKeyDown(KeyCode.Alpha1)){
			player.GetComponent<Pawn>().EquipWeapon(weaponPrefab);
		}
	}
}
