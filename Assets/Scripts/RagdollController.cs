﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour {
	public bool isRagdoll;
	// On When Controlled, Off when ragdoll
	public Animator anim;
	public Collider mainBody;
	public Rigidbody mainRigidbody;
	public Pawn pawn;
	// On when ragdoll, off when controlled.
	public List<Collider> ragdollColliders;
	public List<Rigidbody> ragdollRigidbody;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		mainBody = GetComponent<Collider> ();
		mainRigidbody = GetComponent<Rigidbody> ();
		pawn = GetComponent<Pawn> ();
		ragdollColliders = new List<Collider> (GetComponentsInChildren<Collider> ());
		ragdollColliders.Remove (mainBody);
		ragdollRigidbody = new List<Rigidbody> (GetComponentsInChildren<Rigidbody> ());
		ragdollRigidbody.Remove (mainRigidbody);
		isRagdoll = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (isRagdoll) {
			RagdollOn ();
		} else {
			RagdollOff ();
		}
	}

	void RagdollOn(){
		foreach (Collider col in ragdollColliders) {
			col.enabled = true;
		}
		foreach (Rigidbody rb in ragdollRigidbody) {
			rb.isKinematic = false;
		}
		mainBody.enabled = false;
		mainRigidbody.isKinematic = true;
		anim.enabled = false;
	}

	void RagdollOff(){
		foreach (Collider col in ragdollColliders) {
			col.enabled = false;
		}
		foreach (Rigidbody rb in ragdollRigidbody) {
			rb.isKinematic = true;
		}
		mainBody.enabled = true;
		mainRigidbody.isKinematic = false;
		anim.enabled = true;
	}
}
