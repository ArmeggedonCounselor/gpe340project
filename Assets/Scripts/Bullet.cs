﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	[HideInInspector]
	public float damage = 1;

	public void SetDamage(float dam){
		damage = dam;
	}

	void OnCollisionEnter(Collision other){
		if(!other.collider.gameObject.CompareTag("Bullet")){
		other.collider.gameObject.BroadcastMessage ("TakeDamage", -1 * damage, SendMessageOptions.DontRequireReceiver);
		Destroy (gameObject);
		}
	}
}
