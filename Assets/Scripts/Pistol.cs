﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon {

	// Use this for initialization
	void Start () {
		RightHand = true;
		LeftHand = true;
	}

	// Update is called once per frame
	void Update () {
		
	}

	public override void Fire ()
	{
		// Find the point "spread" units in front of my gun fire position.
		float xSpread = Random.Range (-1f, 1f);
		float ySpread = Random.Range (-1f, 1f);

		Vector3 randomSpread = new Vector3 (xSpread, ySpread, 0.0f).normalized * spread;

		Quaternion rot = Quaternion.Euler (randomSpread) * firePosition.rotation;
		// Instantiate bullet.
		GameObject bullet = Instantiate (bulletPrefab, firePosition.position, rot) as GameObject;

		bullet.BroadcastMessage ("SetDamage", bulletDamage);

		// Push bullet forward.
		Rigidbody bulletBody = bullet.GetComponent<Rigidbody> ();
		bulletBody.AddForce (bullet.transform.forward * bulletForce);

		// Bullet Lifespan.
		Destroy (bullet, bulletLifespan);
	}
}
