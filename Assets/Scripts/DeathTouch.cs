﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTouch : MonoBehaviour {

	[Header("Data")]
	[Tooltip("How much damage this object does on contact.")]
	public float damage;
	[Tooltip("How quickly this object repeats hits while in contact.")]
	public float hurtTimer;

	public float timer;

	void Start(){
		timer = 6.0f;
	}

	void Update(){
		if (timer < hurtTimer) {
			timer += Time.deltaTime;
		}
	}

	public void OnCollisionStay(Collision other){
		if (timer >= hurtTimer) {
			timer = 0.0f;
			other.collider.gameObject.BroadcastMessage ("TakeDamage", -1 * damage, SendMessageOptions.DontRequireReceiver);
		}
	}
}
