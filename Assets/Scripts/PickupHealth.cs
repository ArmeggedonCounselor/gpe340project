﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupHealth : Pickup {

	[Header("Data")]
	[SerializeField][Tooltip("The amount of health this pickup heals.")] private float healAmount;

	// Use this for initialization
	public override void Start () {

		base.Start ();
	}
	
	// Update is called once per frame
	public override void Update () {

		base.Update ();
	}

	/// <summary>
	/// Heals the player when they pickup the object.
	/// </summary>
	public override void OnPickup(GameObject target){
		Health tempHealth = target.GetComponent<Health> ();
		if (tempHealth != null) {
			tempHealth.TakeDamage (healAmount);
		}

		Pawn tempPawn = target.GetComponent<Pawn> ();
		if (tempPawn != null) {
			base.OnPickup (target);
		}
	}
}
