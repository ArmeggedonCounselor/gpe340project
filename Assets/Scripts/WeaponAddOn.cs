﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WeaponAddOn : AddOn {

	[Header("Prefabs")]
	[Tooltip("The bullet this Add-On fires.")] public GameObject bulletPrefab;
	[Tooltip("The hit particles this Add-On creates.")] public GameObject hitParticlePrefab;
	[Tooltip("The line that this object's hitscan makes.")] public GameObject linePrefab;

	[Header("Components")]
	[Tooltip("The point where bullets come out of the Add-On.")]public Transform firePosition;

	[Header("Data")]
	[Tooltip("How far the Add-On scans to find a target.")]public float scanRange;
	[Tooltip("Max distance before Add-On retargets. Cannot be larger than weapon range.")] public float retargetRange;
	[Tooltip("How often the Add-On fires its burst.")] public float burstTimer;
	[Tooltip("How large the Add-On's burst is.")] public int burstCount;
	[Tooltip("How fast the Add-On turns.")] public float turnSpeed;

	[Tooltip("The force of the bullets fired out of this Add-On.")] public float bulletForce;
	[Tooltip("When to destroy the bullet.")] public float bulletLifespan;
	[Tooltip("How long particles should stay around.")] public float particleLifespan;
	[Tooltip("Max range of the Add-On.")] public float fireDistance;
	[Tooltip("Damage the bullet does.")] public float bulletDamage;
	[Tooltip("Spread of the gun.")] public float spread;

	private GameObject target;
	private Transform targetPosition;
	private Transform myPosition;
	private float targetDistance;
	private float burstTiming;
	private int burstCounter;



	// Use this for initialization
	void Start () {
		myPosition = gameObject.transform;
		burstTiming = burstTimer;
		burstCounter = 0;
		if (retargetRange.CompareTo (fireDistance) > 0) {
			retargetRange = fireDistance;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
			UpdateDistance ();
			if (CheckDistance ()) {
				RotateTowards ();
				if (burstTiming >= burstTimer) {
					Fire ();
					burstCounter++;
					if (burstCounter == burstCount) {
						burstTiming = 0;
						burstCounter = 0;
					}
				}
			} else {
				target = null;
			}
		} else {
			AcquireTarget ();
			burstTiming = 0;
			burstCounter = 0;
		}
		if (burstTiming < burstTimer) {
			burstTiming += Time.deltaTime;
		}
	}

	void AcquireTarget(){
		List<Collider> targets = new List<Collider> ();
		Collider closest;
		int layermask = 1 << LayerMask.NameToLayer ("Add-On Ignore");
		layermask = ~layermask;
		targets = Physics.OverlapSphere (myPosition.position, scanRange, layermask).ToList ();
		if (targets.Count > 0) {
			closest = targets [0];
			Vector3 closestPosition = closest.gameObject.transform.position;
			float distance = Vector3.Distance (myPosition.position, closestPosition);
			if (targets.Count > 1) {
				foreach (Collider ctarget in targets) {
					float newDistance = Vector3.Distance (myPosition.position, ctarget.gameObject.transform.position);
					if (newDistance.CompareTo (distance) < 0) {
						closest = ctarget;
						closestPosition = closest.gameObject.transform.position;
						distance = newDistance;
					}
				}
			}

			target = closest.gameObject;
			targetPosition = closest.transform;
			targetDistance = distance;
		}
	}

	void UpdateDistance(){
		targetDistance = Vector3.Distance (myPosition.position, targetPosition.position);
	}

	bool CheckDistance(){
		return (targetDistance.CompareTo (retargetRange) < 0);
	}

	void RotateTowards(){
		Vector3 targetVector = targetPosition.position - myPosition.position;

		Quaternion targetRotation = Quaternion.LookRotation (targetVector);

		myPosition.rotation = Quaternion.RotateTowards (myPosition.rotation, targetRotation, turnSpeed);
	}

	void Fire(){
		float xSpread = Random.Range (-1f, 1f);
		float ySpread = Random.Range (-1f, 1f);

		Vector3 randomSpread = new Vector3 (xSpread, ySpread, 0.0f).normalized * spread;

		Quaternion rot = Quaternion.Euler (randomSpread) * firePosition.rotation;
		Ray clearVector = new Ray ();
		clearVector.origin = firePosition.position;
		clearVector.direction = firePosition.forward;
		RaycastHit hitData;
		if (Physics.Raycast (clearVector, out hitData, fireDistance)) {
			GameObject hit = hitData.collider.gameObject;
			if (hit == target) {
				// Instantiate bullet.
				GameObject bullet = Instantiate (bulletPrefab, firePosition.position, rot) as GameObject;
				bullet.BroadcastMessage ("SetDamage", bulletDamage);

				// Push bullet forward.
				Rigidbody bulletBody = bullet.GetComponent<Rigidbody> ();
				bulletBody.AddForce (bullet.transform.forward * bulletForce);

				// Bullet Lifespan.
				Destroy (bullet, bulletLifespan);
			}
		}
	}
}
