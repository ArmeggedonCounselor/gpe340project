﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	[Header("Inverse Kinematics")]
	[Tooltip("Does this weapon have a left hand IK Node?")]public bool LeftHand = false;
	[Tooltip("Does this weapon have a right hand IK Node?")]public bool RightHand = false;
	[Tooltip("The Transform for the right-hand IK Node.")]public Transform RHPoint;
	[Tooltip("The Transform for the left-hand IK Node.")]public Transform LHPoint;

	[Header("Prefabs")]
	[Tooltip("The bullet this weapon fires.")] public GameObject bulletPrefab;
	[Tooltip("The hit particles this weapon creates.")] public GameObject hitParticlePrefab;
	[Tooltip("The line that this object's hitscan makes.")] public GameObject linePrefab;

	[Header("Components")]
	[Tooltip("The point where bullets come out of the weapon.")]public Transform firePosition;

	[Header("Data")]
	[Tooltip("The force of the bullets fired out of this weapon.")] public float bulletForce;
	[Tooltip("When to destroy the bullet.")] public float bulletLifespan;
	[Tooltip("How long particles should stay around.")] public float particleLifespan;
	[Tooltip("Max range of the weapon.")] public float fireDistance;
	[Tooltip("Damage the bullet does.")] public float bulletDamage;
	[Tooltip("Spread of the gun.")] public float spread;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Occurs when the player chooses to equip this weapon.
	/// </summary>
	public virtual void OnEquip(){
		//TODO: Play sound, update UI.
	}

	/// <summary>
	/// What happens when the trigger is pulled.
	/// </summary>
	public virtual void PullTrigger(){
		//TODO: Play sound, update UI, muzzle flash, maybe?
		Fire ();
	}

	/// <summary>
	/// What happens when the weapon fires.
	/// </summary>
	public virtual void Fire(){/*
		// Find the point "spread" units in front of my gun fire position.
		Vector3 newPoint = firePosition.position + (firePosition.forward * spread);

		// Find a random point within a sphere "spread" units in size.
		Vector3 randomPoint = Random.insideUnitSphere;
		randomPoint = randomPoint * spread;

		// Move that point over our "in front of" point.
		randomPoint = newPoint + randomPoint;

		// Instantiate bullet.
		GameObject bullet = Instantiate (bulletPrefab, firePosition.position, firePosition.rotation) as GameObject;

		// Point our bullet towards our random point
		bullet.transform.LookAt(randomPoint);

		// Push bullet forward.
		Rigidbody bulletBody = bullet.GetComponent<Rigidbody> ();
		bulletBody.AddForce (bullet.transform.forward * bulletForce);

		// Bullet Lifespan.
		Destroy (bullet, bulletLifespan);
		*/
	}

	/// <summary>
	/// What happens when you stop firing the weapon.
	/// </summary>
	public virtual void ReleaseTrigger(){
		//TODO: Stop playing gun noises. Maybe some other effects for special guns or bows.
	}


}
