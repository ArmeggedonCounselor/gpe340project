﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {

	[HideInInspector]
	public Transform tf;
	[HideInInspector]
	public bool isDead = false;
	[HideInInspector]
	public bool isCrouching = false;

	void Awake(){
		tf = GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		
	}
	/// <summary>
	/// Moves along the specified vector.
	/// </summary>
	/// <param name="moveVector">The vector along which the pawn will move. </param>
	public virtual void Move (Vector2 moveVector){}

	/// <summary>
	/// Moves without having a specific vector - for use with NavMesh.
	/// </summary>
	public virtual void Move(){}

	/// <summary>
	/// Do whatever is necessary on death, then play the death animation.
	/// </summary>
	public virtual void Die (){
	}

	/// <summary>
	/// Rotates the pawn toward some target in space.
	/// </summary>
	/// <param name="target">The world coordinates of the target.</param>
	public virtual void RotateTowards(Vector3 target){
	}

	/// <summary>
	/// Makes the pawn crouch.
	/// </summary>
	public virtual void Crouch(){
	}

	/// <summary>
	/// Equips the specified weapon.
	/// </summary>
	/// <param name="weapon">A game object that inherits the Weapon type.</param>
	public virtual void EquipWeapon(Weapon weapon){
	}

	public virtual void NextWeapon(bool direction){
	}

	public virtual void FireWeapon(){
	}
}
