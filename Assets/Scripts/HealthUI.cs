﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {
	[SerializeField]
	[Tooltip("The Text component on the UI this script will control.")]
	private Text UI;

	[SerializeField]
	[Tooltip("The Health component on the GameObject this script will display.")]
	private Health target;

	private float lastHealth;

	// Use this for initialization
	void Start () {
		UI.text = "Health: " + target.healthPercent + "%";
		lastHealth = target.healthPercent;
	}
	
	// Update is called once per frame
	void Update () {
		if (lastHealth != target.healthPercent) {
			UI.text = "Health: " + target.healthPercent + "%";
			lastHealth = target.healthPercent;
		}
	}
}
